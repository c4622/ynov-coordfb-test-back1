import express from "express";

const app = express();

app.use(express.static("front/dist"));

app.listen(process.env.PORT || 8080, function(){
    console.log("Listening");
});